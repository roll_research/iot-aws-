import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;


public class ServletAddServlet extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data

		//writing html in the stream  
		pw.println("<html><body>");
		pw.println("<h1>Welcome to ServletAddServlet</h1>");
		pw.println("<ul>\n" + 
				"		<li><a href=\"/app/\">JSP</a></li>\n" + 
				"		<li><a href=\"/app/servlet-web-xml\">web.xml</a></li>\n" + 
				"		<li><a href=\"/app/servlet-addservlet\">addServlet</a></li>\n" + 
				"	</ul>");
		pw.println("</body></html>");

		pw.close();// closing the stream
	}
	
	public void doPost(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException{
		System.out.println("ServletAddServlet");
		
		//res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data
		
		//login_info login = new login_info();
		//DB_Connection connection = new DB_Connection();
		
		String user_id=req.getParameter("name");
		String user_password=req.getParameter("age");
		
		//login=connection.Connection_login_user(user_id, user_password);
		
		//pw.println("user_id:"+login.getUser_id());
		//pw.println(",user_password:"+login.getUser_pw());
		//pw.println(",aircleaner_id:"+login.getAircleaner_id());
		
		pw.println("name:"+user_id);
		pw.println(",age:"+user_password);
		pw.close();
	}
}
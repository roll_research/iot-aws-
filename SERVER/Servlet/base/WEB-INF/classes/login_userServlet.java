import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class login_userServlet extends HttpServlet{

	public void doGet(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data

		//writing html in the stream  
		pw.println("<html><body>");
		pw.println("<h1>Welcome to login_userServlet</h1>");
		pw.println("<ul>\n" + 
				"		<li><a href=\"/app/\">JSP</a></li>\n" + 
				"		<li><a href=\"/app/servlet-web-xml\">web.xml</a></li>\n" + 
				"		<li><a href=\"/app/servlet-addservlet\">addServlet</a></li>\n" + 
				"	</ul>");
		pw.println("</body></html>");

		pw.close();// closing the stream
	}
	
	public void doPost(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException{
		System.out.println("login_user servlet");
		
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html;charset=utf-8");// setting the content type
		
		//res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data
		
		login_info login = new login_info();
		DB_Connection connection = new DB_Connection();
		
		String user_id=req.getParameter("user_id");
		String user_password=req.getParameter("user_password");
		
		System.out.println(user_id+","+user_password);
		
		login=connection.Connection_login_user(user_id, user_password);
		
		if(login.getUser_id().equals("empty")||login.getUser_pw().equals("empty")||login.getAircleaner_id()==0) {
			pw.println("result:NO,");
			pw.println(",user_id:"+login.getUser_id());
			pw.println(",user_password:"+login.getUser_pw());
			pw.println(",aircleaner_id:"+login.getAircleaner_id());
			pw.close();
		}
		else 
		{
			pw.println("result:OK");
			pw.println(",user_id:"+login.getUser_id());
			pw.println(",user_password:"+login.getUser_pw());
			pw.println(",aircleaner_id:"+login.getAircleaner_id());
			pw.close();
		}
	}

}

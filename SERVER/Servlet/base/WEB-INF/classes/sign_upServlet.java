import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class sign_upServlet extends HttpServlet{
	public void doGet(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data

		//writing html in the stream  
		pw.println("<html><body>");
		pw.println("<h1>Welcome to sign_upServlet</h1>");
		pw.println("<ul>\n" + 
				"		<li><a href=\"/app/\">JSP</a></li>\n" + 
				"		<li><a href=\"/app/servlet-web-xml\">web.xml</a></li>\n" + 
				"		<li><a href=\"/app/servlet-addservlet\">addServlet</a></li>\n" + 
				"	</ul>");
		pw.println("</body></html>");

		pw.close();// closing the stream
	}
	
	public void doPost(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException{
		System.out.println("sign_upServlet");
		
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html;charset=utf-8");// setting the content type
		
		//res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data
		
		DB_Connection connection = new DB_Connection();
		
		String result = null;
		
		String user_id=req.getParameter("user_id");
		String user_password=req.getParameter("user_password");
		String user_name=req.getParameter("user_name");
		String region = req.getParameter("region");
		int user_phon_number = Integer.parseInt(req.getParameter("user_phon_number"));
		int aircleaner_id = Integer.parseInt(req.getParameter("aircleaner_id"));
		
		result=connection.Connection_Signup_user(user_id, user_password, user_name, region, user_phon_number, aircleaner_id);
		
		pw.println("Result:"+result);
		pw.close();
	}
}

public class user_info {
	private String user_id;
	private String user_password;
	private String user_name;
	private int phon_number;
	private int aircleaner_id;
	private String region;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public int getPhon_number() {
		return phon_number;
	}
	public void setPhon_number(int phon_number) {
		this.phon_number = phon_number;
	}
	public int getAircleaner_id() {
		return aircleaner_id;
	}
	public void setAircleaner_id(int aircleaner_id) {
		this.aircleaner_id = aircleaner_id;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	
}

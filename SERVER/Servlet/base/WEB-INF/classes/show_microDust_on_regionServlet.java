import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class show_microDust_on_regionServlet extends HttpServlet{
	public void doGet(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data

		//writing html in the stream  
		pw.println("<html><body>");
		pw.println("<h1>Welcome to show_microDust_on_regionServlet</h1>");
		pw.println("<ul>\n" + 
				"		<li><a href=\"/app/\">JSP</a></li>\n" + 
				"		<li><a href=\"/app/servlet-web-xml\">web.xml</a></li>\n" + 
				"		<li><a href=\"/app/servlet-addservlet\">addServlet</a></li>\n" + 
				"	</ul>");
		pw.println("</body></html>");

		pw.close();// closing the stream
	}
	
	public void doPost(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException{
		System.out.println("show_microDust_on_regionServlet");
		
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html;charset=utf-8");// setting the content type
		
		//res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data
		
		outdoor_dust_info[] dust_info = new outdoor_dust_info[12];
		
		DB_Connection connection = new DB_Connection();
		
		String result = "";
		
		int aircleaner_id = Integer.parseInt(req.getParameter("aircleaner_id"));
		
		try {
			dust_info=connection.Connection_Outdoor_info_sending(aircleaner_id);
			
			if(dust_info[1].getOutdoor_day_time().equals("empty")||dust_info[1].getOutdoor_dust_info()==-1||dust_info[1].getOutdoor_micro_dust_info()==-1||dust_info[1].getRegion().equals("empty")) {
				result= "result:NO/";
			}else {
				result= "result:OK/";
			}
			
			for(int i =0;i<12;i++) {
				result=result+dust_info[i].getOutdoor_day_time()+"/"+dust_info[i].getOutdoor_dust_info()
				+"/"+dust_info[i].getOutdoor_micro_dust_info()+"/"+dust_info[i].getRegion();
				if(i<11) {
					result = result+",";
				}
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			result = "result:NO";
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			result = "result:NO";
			e.printStackTrace();
		}
		
		pw.println(result);
		pw.close();
	}
}
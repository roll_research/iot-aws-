
public class signup_info {
	private String user_id;
	private String password;
	private String user_name;
	private int phon_number;
	private int aircleaner;
	private String region;
	
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public int getPhon_number() {
		return phon_number;
	}
	public void setPhon_number(int phon_number) {
		this.phon_number = phon_number;
	}
	public int getAircleaner() {
		return aircleaner;
	}
	public void setAircleaner(int aircleaner) {
		this.aircleaner = aircleaner;
	}
	
	
}
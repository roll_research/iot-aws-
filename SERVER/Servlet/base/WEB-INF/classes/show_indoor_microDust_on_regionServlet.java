import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class show_indoor_microDust_on_regionServlet extends HttpServlet{
	public void doGet(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data

		//writing html in the stream  
		pw.println("<html><body>");
		pw.println("<h1>Welcome to show_indoor_microDust_on_regionServlet</h1>");
		pw.println("<ul>\n" + 
				"		<li><a href=\"/app/\">JSP</a></li>\n" + 
				"		<li><a href=\"/app/servlet-web-xml\">web.xml</a></li>\n" + 
				"		<li><a href=\"/app/servlet-addservlet\">addServlet</a></li>\n" + 
				"	</ul>");
		pw.println("</body></html>");

		pw.close();// closing the stream
	}
	
	public void doPost(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException{
		System.out.println("show_indoor_microDust_on_regionServlet");
		
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html;charset=utf-8");// setting the content type
		
		//res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data
		
		indoor_dust_info_A[] dust_info = new indoor_dust_info_A[12];
		
		DB_Connection connection = new DB_Connection();
		
		String result = "";
		
		int aircleaner_id = Integer.parseInt(req.getParameter("aircleaner_id"));
		
		//String name=req.getParameter("user_id");
		//String age=req.getParameter("user_password");
		
		try {
			dust_info=connection.Connection_Indoor_info_sending(aircleaner_id);
			
			if(dust_info[1].getIndoor_day_time().equals("empty")||dust_info[1].getIndoor_dust_info()==-1||dust_info[1].getAircleaner_number()==0) {
				result = "result:NO/";
			}else {
				result = "result:OK/";
			}
			
			
			for(int i =0;i<12;i++) {
				result=result+dust_info[i].getIndoor_day_time()+"/"+dust_info[i].getIndoor_dust_info()
				+"/"+dust_info[i].getAircleaner_number();
				if(i<11) {
					result = result+",";
				}
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			result = "result:NO";
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			result = "result:NO";
			e.printStackTrace();
		}
		
		
		pw.println(result);
		pw.close();
	}
}

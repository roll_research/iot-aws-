import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;

public class DB_Connection {
	
	public login_info Connection_login_user(String req_user_id, String req_user_password) {
		Connection connection =null;
	    Statement st = null;
	    String sql;
	    ResultSet rs;
	    
		String user_id = "empty";
		String user_pw = "empty";
		String ua_id =null;
		int aircleaner_id =0;
		
		login_info save_info = new login_info();
		
			
		System.out.println("작동시작");
		try { 
		  
		  Class.forName("com.mysql.cj.jdbc.Driver");
		  connection = DriverManager.getConnection
		  ("jdbc:mysql://management.co21pcqhekw5.us-east-1.rds.amazonaws.com:3306/management?"
		  	+ "useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "user","dmschd5884");
				  
		  //("jdbc:mysql://localhost/android_project?useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "root","rusy5884!@runner");
		  
		  //("jdbc:mysql://localhost/simpledb?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC","root", "rusy5884!@runner");
        
		st = connection.createStatement();
        sql = "select * FROM user_info;";
        rs = st.executeQuery(sql);
        
        System.out.println(req_user_id);
        System.out.println(req_user_password);
        
        while (rs.next()) {
            user_id = rs.getString("id");
			user_pw = rs.getString("pw");
			System.out.println(user_id + user_pw);
			if(req_user_id.equals(user_id)&&req_user_password.equals(user_pw)==true) {
				System.out.println("해당 아이디 정보를 찾았습니다.");
				System.out.println("user_id: "+user_id +" user_pw: "+user_pw);
				break;
			}
			user_id="empty";
			user_pw="empty";
        }
        
        sql = "select * FROM user_and_aircleaner;";
        rs = st.executeQuery(sql);

        while (rs.next()) {
            ua_id = rs.getString("id");
			aircleaner_id = rs.getInt("aircleaner_id");
			if(ua_id.equals(user_id)==true) {
				System.out.println("해당 아이디 정보를 찾았습니다.공기청정기 번호를 가져옵니다.");
				System.out.println("aircleaner_id:"+aircleaner_id);
				save_info.setUser_id(user_id);
		        save_info.setUser_pw(user_pw);
		        save_info.setAircleaner_id(aircleaner_id);
				return save_info;
			}
        }
        
        save_info.setUser_id("empty");
    	save_info.setUser_pw("empty");
    	save_info.setAircleaner_id(0);
        
        rs.close();
        st.close();
        connection.close();
        
    } 
	catch (SQLException se1) {
  	  System.out.println(se1.toString());
  	  save_info.setUser_id("empty");
  	  save_info.setUser_pw("empty");
  	  save_info.setAircleaner_id(0);
  	  
    } 
	catch (Exception ex) {
  	 System.out.println(ex.toString());
  	  save_info.setUser_id("empty");
	  save_info.setUser_pw("empty");
	  save_info.setAircleaner_id(0);
    } 
	finally {
       try {
            if (st != null)
                st.close();
       	} 
        catch (SQLException se2) {
      	  System.out.println(se2.toString());
      	  save_info.setUser_id("empty");
    	  save_info.setUser_pw("empty");
    	  save_info.setAircleaner_id(0);
        }
        try {
            if (connection != null)
          	  System.out.println("작동중지");
                connection.close();
        } catch (SQLException se) {
            //se.printStackTrace();
      	  System.out.println(se.toString());
      	  save_info.setUser_id("empty");
    	  save_info.setUser_pw("empty");
    	  save_info.setAircleaner_id(0);
        }
    }
		return save_info;	
	}
	
	public String Connection_Signup_user(String req_user_id, String req_user_password, String req_user_name,String req_region ,int req_user_phon_number,int req_aircleaner_id) {
		//결과 정보
		String Result_Check = null;
		
		// 삽입 정보 관련 자료형들
		Connection connection_insert =null;
	    PreparedStatement st_insert = null;
		String sql = null;
			
		// 읽어옴 정보 관련 자료형들
		Connection connection_checking =null;
	    Statement st_checking = null;
		
		//업데이트를 위한 자료형들
		Connection connection_update = null;
		PreparedStatement st_update = null;
		String upsqlStr = null;
		
		//공기청정기 정보들을 저장하는 자료형들
	    int aircleaner_id =0;
	    int action_motion_number =0;
		
		signup_info signup = new signup_info();
	    
		System.out.println("작동시작");
		try { 
		  
		  //회원가입 테이블 삽입
			
		  Class.forName("com.mysql.cj.jdbc.Driver");
		  connection_insert = DriverManager.getConnection
		  ("jdbc:mysql://management.co21pcqhekw5.us-east-1.rds.amazonaws.com:3306/management?"
		  	+ "useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "user","dmschd5884");
		 
		  sql = "insert into user_info values(?, ?, ?, ?);";
		  
		  st_insert = connection_insert.prepareStatement(sql);
		  
		  System.out.println(req_user_id+" "+req_user_password+" "+req_user_name+" "+req_user_phon_number);
		  
		  st_insert.setString(1, req_user_id);
		  st_insert.setString(2, req_user_password);
		  st_insert.setString(3, req_user_name);
		  st_insert.setInt(4, req_user_phon_number);
		  st_insert.executeUpdate();
		  System.out.println("회원정보가 입력되었습니다.");
		  
        
        //System.out.println("작동끝");
        st_insert.close();
        connection_insert.close();
        
        //회원_공기청정기 정보에 삽입
        
        Class.forName("com.mysql.cj.jdbc.Driver");
		  connection_insert = DriverManager.getConnection
		  ("jdbc:mysql://management.co21pcqhekw5.us-east-1.rds.amazonaws.com:3306/management?"
		  	+ "useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "user","dmschd5884");
		 
		  sql = "insert into user_and_aircleaner values(?, ?);";
		  
		  st_insert = connection_insert.prepareStatement(sql);
		  
		  st_insert.setString(1, req_user_id);
		  st_insert.setInt(2, req_aircleaner_id);
		  st_insert.executeUpdate();
		  System.out.println("회원_공기청정기 정보가 입력되었습니다.");
		  
      
		  //System.out.println("작동끝");
		  st_insert.close();
		  connection_insert.close();
      
      
      //공기청정기 정보를 불려옴
	
	  //System.out.println("작동시작");
	  
	  Class.forName("com.mysql.cj.jdbc.Driver");
	  connection_checking = DriverManager.getConnection
      ("jdbc:mysql://localhost/final_project?useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "root","rusy5884!@runner");
	  
	  //("jdbc:mysql://localhost/simpledb?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC","root", "rusy5884!@runner");
      
	  st_checking = connection_checking.createStatement();
      
      sql = "select * FROM aircleaner;";

      ResultSet rs = st_checking.executeQuery(sql);
      
      //int aircleaner_id =0;
      //int action_motion_number =0;
      
      while (rs.next()) {
          	aircleaner_id = rs.getInt("aircleaner_id");
			action_motion_number = rs.getInt("action_motion_number");
			if(aircleaner_id ==req_aircleaner_id) {
				System.out.println("공기청정기의 모션 정보 번호: "+action_motion_number);
				break;
			}
      }
      
      //System.out.println("작동끝");
      rs.close();
      st_checking.close();
      connection_checking.close();
      
      // 수동동작에 지역구 추가하기
      
      Class.forName("com.mysql.cj.jdbc.Driver");
	  connection_update = DriverManager.getConnection
	  ("jdbc:mysql://localhost/final_project?useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "root","rusy5884!@runner");
	 
	  sql = "UPDATE manual_acting_info SET region=?";
	  sql  = sql + " WHERE share_acting_number=?";
	  
	  //String sql = " UPDATE predb SET language=?, db=?, os=? ";
      // sql  = sql + " WHERE action_motion_number=?";
	  
	  st_update = connection_update.prepareStatement(sql);
	  
	  st_update.setString(1, req_region);
	  st_update.setInt(2, action_motion_number);
	  st_update.executeUpdate();
	  System.out.println("지역 정보가 입력되었습니다.");
      
	  st_update.close();
      connection_update.close();
      
      
      signup.setUser_id(req_user_id);
      signup.setPassword(req_user_password);
      signup.setPhon_number(req_user_phon_number);
      signup.setRegion(req_region);
      signup.setUser_name(req_user_name);
      signup.setAircleaner(req_aircleaner_id);
      
      String aircleaner_toString = Integer.toString(signup.getAircleaner());
      
      Result_Check ="OK"+"/"+signup.getUser_id()+"/"+signup.getPassword()+"/"+signup.getUser_name()
      +"/"+signup.getRegion()+"/"+signup.getPhon_number()+"/"+aircleaner_toString;
        
    } catch (SQLException se1) {
        //se1.printStackTrace();
  	  	System.out.println(se1.toString());
  	  	Result_Check = "NO/asdf";
        
    } catch (Exception ex) {
        //ex.printStackTrace();
    	System.out.println(ex.toString());
  		Result_Check = "NO/asdf";
    } finally {
        try {
            if (st_checking != null)
                st_checking.close();
            if (st_insert != null)
                st_insert.close();
            if (st_update != null)
                st_update.close();
        } catch (SQLException se2) {
      	  System.out.println(se2.toString());
      	  Result_Check = "NO/asdf";
        }
        try {
            if (connection_checking != null) {
          	  System.out.println("작동중지");
                connection_checking.close();
            }
            if (connection_insert != null) {
            	  System.out.println("작동중지");
                  connection_insert.close();
              }
            if (connection_update != null) {
            	  System.out.println("작동중지");
                  connection_update.close();
              }
        } catch (SQLException se) {
            //se.printStackTrace();
      	  System.out.println(se.toString());
      	  Result_Check = "NO/asdf";
        }
    }
		return  Result_Check;
	}
	
	public outdoor_dust_info[] Connection_Outdoor_info_sending(int req_aircleaner_id) throws ClassNotFoundException, SQLException {
		
		//디비 떄에 사용이 필요한 변수들
		Connection connection =null;
	    //PreparedStatement st_pre = null;
	    Statement st = null;
	    ResultSet rs = null;
		String sql = null;
		
		//공기청정기 정보를 뽑을 때 필요한 정보들
		int aircleaner_id = 0;
		int action_motion_number=0;
		
		//수동동작 관련 정보를 뽑을 떄 필요한 정보들
		int share_acting_number = 0;
		String region = null;
		
		String outdoor_day_time = null;
		float outdoor_dust_info = 0;
		float outdoor_micro_dust_info =0;
		String region_outdoor =null;
		
		//야외 미세먼지 정보 클레스
		outdoor_dust_info[] out_dust_info = new outdoor_dust_info[12];
		
		for(int i=0; i<12;i++) {
			out_dust_info[i] = new outdoor_dust_info();
			out_dust_info[i].setOutdoor_day_time("empty");
			out_dust_info[i].setOutdoor_dust_info(-1);
			out_dust_info[i].setOutdoor_micro_dust_info(-1);
			out_dust_info[i].setRegion("empty");
		}
		
		int count = 0;
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		  connection = DriverManager.getConnection
	      ("jdbc:mysql://localhost/final_project?useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "root","rusy5884!@runner");
		  
		  //("jdbc:mysql://localhost/simpledb?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC","root", "rusy5884!@runner");
	      
		  st = connection.createStatement();
	      
	      sql = "select * FROM aircleaner;";

	      rs = st.executeQuery(sql);
	      
	      //int aircleaner_id =0;
	      //int action_motion_number =0;
	      
	      while (rs.next()) {
	          	aircleaner_id = rs.getInt("aircleaner_id");
				action_motion_number = rs.getInt("action_motion_number");
				if(aircleaner_id ==req_aircleaner_id) {
					System.out.println("공기청정기의 모션 정보 번호: "+action_motion_number);
					break;
				}
	      }
	      System.out.println("공기청정기 모션 정보를 찾았습니다.");
	      
	      sql = "select * FROM manual_acting_info;";
	      
	      rs = st.executeQuery(sql);
	      
	      while (rs.next()) {
	    	  	share_acting_number = rs.getInt("share_acting_number");
	    	  	region = rs.getString("region");
				if(share_acting_number==action_motion_number) {
					System.out.println("해당 지역 찾음: "+region);
					break;
				}
	      }
	      
	      sql = "select * FROM outdoor_info where region="+"'"+region+"'"+"order by outdoor_info_number desc;";
	      
	      rs = st.executeQuery(sql);
	      
	      while (rs.next()) {
	    	  	outdoor_day_time = rs.getString("outdoor_day_time");
	    	  	outdoor_dust_info = rs.getFloat("outdoor_dust_info");
	    	  	outdoor_micro_dust_info =rs.getFloat("outdoor_micro_dust_info");
	    	  	region_outdoor =rs.getString("region");
				if(region_outdoor.equals(region)) {
					System.out.println("해당 야외 미세먼지 정보 찾음:");
					out_dust_info[count].setOutdoor_day_time(outdoor_day_time);
					out_dust_info[count].setOutdoor_dust_info(outdoor_dust_info);
					out_dust_info[count].setOutdoor_micro_dust_info(outdoor_micro_dust_info);
					out_dust_info[count].setRegion(region_outdoor);
					if(count<11) {
						count++;
					}else {
						return out_dust_info;
						//break;
						
					}
				}
	      }
	      
	      //System.out.println("작동끝");
	      rs.close();
	      st.close();
	      connection.close();
	      
	      return out_dust_info;
	}

	public outdoor_dust_info[] connection_OutdoorDust_info_sending_region(String region) throws ClassNotFoundException, SQLException{
		
		System.out.println("외부 지역 정보 확인합니다");
		//디비 떄에 사용이 필요한 변수들
		Connection connection =null;
		//PreparedStatement st_pre = null;
		Statement st = null;
		ResultSet rs = null;
		String sql = null;
		
		String outdoor_day_time = null;
		float outdoor_dust_info = 0;
		float outdoor_micro_dust_info =0;
		String region_outdoor =null;
		
		//야외 미세먼지 정보 클레스
		outdoor_dust_info[] out_dust_info = new outdoor_dust_info[12];
		int count = 0;
		
		//for(int i = 0 ; i<12; i++) {
		//	out_dust_info[i] = new outdoor_dust_info();
		//}
		
		for(int i=0; i<12;i++) {
			out_dust_info[i] = new outdoor_dust_info();
			out_dust_info[i].setOutdoor_day_time("empty");
			out_dust_info[i].setOutdoor_dust_info(-1);
			out_dust_info[i].setOutdoor_micro_dust_info(-1);
			out_dust_info[i].setRegion("empty");
		}
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection = DriverManager.getConnection
	    ("jdbc:mysql://localhost/final_project?useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "root","rusy5884!@runner");
		  
		//("jdbc:mysql://localhost/simpledb?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC","root", "rusy5884!@runner");
	      
		st = connection.createStatement();
		
		 sql = "select * FROM outdoor_info where region="+"\'"+region+"\'"+"order by outdoor_info_number desc;";
		
		  
		  System.out.println(sql);
			
	      rs = st.executeQuery(sql);
	      
	      System.out.println("데이터베이스까지 접근했습니다.");
	      
	      while (rs.next()) {
	    	  	System.out.println("데이터를 확입니다.");
	    	  	outdoor_day_time = rs.getString("outdoor_day_time");
	    	  	System.out.println(outdoor_day_time);
	    	  	outdoor_dust_info = rs.getFloat("outdoor_dust_info");
	    	  	System.out.println(outdoor_dust_info);
	    	  	outdoor_micro_dust_info =rs.getFloat("outdoor_micro_dust_info");
	    	  	System.out.println(outdoor_micro_dust_info);
	    	  	region_outdoor =rs.getString("region");
	    	  	System.out.println(region_outdoor);
				if(region_outdoor.equals(region)) {
					System.out.println("해당 지역 찾음: "+region);
					out_dust_info[count].setOutdoor_day_time(outdoor_day_time);
					out_dust_info[count].setOutdoor_dust_info(outdoor_dust_info);
					out_dust_info[count].setOutdoor_micro_dust_info(outdoor_micro_dust_info);
					out_dust_info[count].setRegion(region_outdoor);
					System.out.println(count);
					if(count<11) {
						System.out.println("조건문에 들어갔습니다.");
						count++;
					}else {
						return out_dust_info;
						//break;
						
					}
				}
	      }
	      
	      //System.out.println("작동끝");
	      rs.close();
	      st.close();
	      connection.close();
	      
	      return out_dust_info;
	}
		
	public indoor_dust_info_A[] Connection_Indoor_info_sending(int req_aircleaner_id)throws ClassNotFoundException,SQLException{
		Connection connection =null;
	    PreparedStatement st_pre = null;
	    Statement st = null;
	    ResultSet rs = null;
		String sql = null;
		
		String indoor_day_time = null;
		float indoor_dust_info = 0;
		int aircleaner_number = 0;
		
		indoor_dust_info_A[] indoor_dust_IA = new indoor_dust_info_A[12];
		int count =0;
		
		for (int i=0; i<12;i++) {
			indoor_dust_IA[i] = new indoor_dust_info_A();
			indoor_dust_IA[i].setIndoor_day_time("empty");
			indoor_dust_IA[i].setIndoor_dust_info(-1);
			indoor_dust_IA[i].setAircleaner_number(0);
		}
		
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		  connection = DriverManager.getConnection
	      ("jdbc:mysql://localhost/final_project?useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "root","rusy5884!@runner");
		  
		  //("jdbc:mysql://localhost/simpledb?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC","root", "rusy5884!@runner");
	      
		  st = connection.createStatement();
	      
	      sql = "select * FROM indoor_info where aircleaner_number="+"\'"+req_aircleaner_id+"\'"+"order by indoor_info_number desc;";

	      rs = st.executeQuery(sql);
	      
	      while (rs.next()) {
	    	    indoor_day_time = rs.getString("indoor_day_time");
	    	    indoor_dust_info =rs.getFloat("indoor_dust_info");
	    	    aircleaner_number = rs.getInt("aircleaner_number");
				if(aircleaner_number ==req_aircleaner_id) {
					if(count<12) {
						indoor_dust_IA[count].setIndoor_day_time(indoor_day_time);
						indoor_dust_IA[count].setIndoor_dust_info(indoor_dust_info);
						indoor_dust_IA[count].setAircleaner_number(aircleaner_number);
						count++;
					}else {
						return indoor_dust_IA;
						//break;
					}
				}
	      }
	      System.out.println("공기청정기 모션 정보를 찾았습니다.");
	      
	      rs.close();
	      st.close();
	      connection.close();
	      
	      return indoor_dust_IA;
	}
	
	public String Connection_Manual_acting_info_saving(String req_user_id, boolean ON_OFF_checking, boolean manual_acting_checking) throws ClassNotFoundException {
		
		String Result_Check = "NO";
		
		manual_acting_info mai = new manual_acting_info();
		
		// 회원_공기청정기 정보 조회
		Connection connection_ua =null;
		Statement st_checking_ua = null;
		String sql = null;
		
		// 읽어옴 정보 관련 자료형들
		Connection connection_checking =null;
	    Statement st_checking = null;
		
		//업데이트를 위한 자료형들
		Connection connection_update = null;
		PreparedStatement st_update = null;
		String upsqlStr = null;
		
		//회원 및 공기청정기 정보를 확인할 때 쓰는 자료형들
	    String user_id =null;
		int aircleaner_id =0;
	    
		//공기청정기 수동 동작 번호 정보
	    int action_motion_number =0;
	    
	    //공기청정기 번호 중간 저장값
	    int temp_aircleaner_id =0;
		
	   try {
	    
	        //회원_공기청정기 정보에 삽입
	        
	        Class.forName("com.mysql.cj.jdbc.Driver");
			connection_ua = DriverManager.getConnection
			("jdbc:mysql://management.co21pcqhekw5.us-east-1.rds.amazonaws.com:3306/management?"
			  + "useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "user","dmschd5884");
			
			      st_checking_ua = connection_ua.createStatement();
			
			      sql = "select * FROM user_and_aircleaner;";
			  
			  	  ResultSet rs_ua = st_checking_ua.executeQuery(sql);
		      	      
				  while (rs_ua.next()) {
		          	user_id = rs_ua.getString("id");
					aircleaner_id = rs_ua.getInt("aircleaner_id");
					if(user_id.equals(req_user_id)) {
						System.out.println("공기청정기의 번호: "+aircleaner_id);
						temp_aircleaner_id = aircleaner_id;
						break;
						}
		      	}
			  System.out.println("공기청정기 번호를 확인하였습니다.");
			  rs_ua.close();
			  st_checking_ua.close();
			  connection_ua.close();  
   
		   
		//공기청정기 조회
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection_checking = DriverManager.getConnection
	    ("jdbc:mysql://localhost/final_project?useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "root","rusy5884!@runner");
		  
		//("jdbc:mysql://localhost/simpledb?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC","root", "rusy5884!@runner");
	      
		st_checking = connection_checking.createStatement();
	      
	    sql = "select * FROM aircleaner;";

	    ResultSet rs = st_checking.executeQuery(sql);
	      
	      //int aircleaner_id =0;
	      //int action_motion_number =0;
	      
	    while (rs.next()) {
	          	aircleaner_id = rs.getInt("aircleaner_id");
				action_motion_number = rs.getInt("action_motion_number");
				if(aircleaner_id ==temp_aircleaner_id) {
					System.out.println("공기청정기의 모션 정보 번호: "+action_motion_number);
					break;
				}
	      }
	      
	     System.out.println("공기청정기의 모션 정보 번호를 확인하였습니다.");
	     rs.close();
	     st_checking.close();
	     connection_checking.close();
	      
	      // 수동동작에 지역구 추가하기
	      
	     Class.forName("com.mysql.cj.jdbc.Driver");
		 connection_update = DriverManager.getConnection
		 ("jdbc:mysql://localhost/final_project?useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "root","rusy5884!@runner");
		 
		 //update manual_acting_info set ON_OFF_checking =true and manual_acting_checking = true
		 
		 sql = "UPDATE manual_acting_info SET ON_OFF_checking=?, manual_acting_checking=?";
		 sql  = sql + " WHERE share_acting_number=?";
		  
		  //String sql = " UPDATE predb SET language=?, db=?, os=? ";
	      // sql  = sql + " WHERE action_motion_number=?";
		  
		 st_update = connection_update.prepareStatement(sql);
		  
		 st_update.setBoolean(1, ON_OFF_checking);
		 st_update.setBoolean(2, manual_acting_checking);
		 st_update.setInt(3, action_motion_number);
		 st_update.executeUpdate();
		 System.out.println("공기청정기 수동작동 설정되었습니다.");
	      
		 
		 mai.setON_OFF_checking(ON_OFF_checking);
		 mai.setManual_acting_checking(manual_acting_checking);
		 
		 st_update.close();
	     connection_update.close();
	      
	     Result_Check = mai.isON_OFF_checking()+"/"+mai.isManual_acting_checking()+"/"+"OK";
	   }catch(SQLException se1) {
		   System.out.println(se1.toString());
		   Result_Check="NO";
	   }
		return Result_Check;
	}

	public user_info Connection_Full_userInfo(String req_user_id) throws ClassNotFoundException, SQLException {
		Connection connection =null;
	    Statement st = null;
	    String sql;
	    ResultSet rs;
	    
		String user_id = "empty";
		String user_pw = "empty";
		String user_name = "empty";
		int phon_number = 0;
		int aircleaner_id =0;
		String region ="empty";
		
		String ua_id =null;
		
		int aircleaner_id2 =0;
		int action_motion_number =0;
		int temp_action_motion_number =0;
		
		int share_acting_number =0;
		
		user_info user = new user_info();
		
		System.out.println("작동시작"); 
		  
		  Class.forName("com.mysql.cj.jdbc.Driver");
		  connection = DriverManager.getConnection
		  ("jdbc:mysql://management.co21pcqhekw5.us-east-1.rds.amazonaws.com:3306/management?"
		  	+ "useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "user","dmschd5884");
				  
		  //("jdbc:mysql://localhost/android_project?useUnicode=true&characterEncoding=utf-8&&serverTimezone=UTC", "root","rusy5884!@runner");
		  
		  //("jdbc:mysql://localhost/simpledb?useUnicode=true&characterEncoding=utf-8&serverTimezone=UTC","root", "rusy5884!@runner");
        
		  st = connection.createStatement();
		  sql = "select * FROM user_info;";
		  rs = st.executeQuery(sql);
        
		  System.out.println(req_user_id);
        
		  while (rs.next()) {
			  user_id = rs.getString("id");
			  user_pw = rs.getString("pw");
			  user_name = rs.getString("user_name");
			  phon_number =rs.getInt("phon_number");
			  System.out.println(user_id +" "+ user_pw+" "+user_name+" "+phon_number);
			  if(req_user_id.equals(user_id)==true) {
				  System.out.println("해당 아이디 정보를 찾았습니다.");
				  System.out.println("user_id: "+user_id +" user_pw: "+user_pw
				  +" user_name: "+user_name+" phon_number: "+phon_number);
				  
				  user.setUser_id(user_id);
				  user.setUser_password(user_pw);
				  user.setUser_name(user_name);
				  user.setPhon_number(phon_number);
				  break;
			  }
			  user_id="empty";
			  user_pw="empty";
			  user_name="empty";
			  phon_number=0;
		  }
        
		  sql = "select * FROM user_and_aircleaner;";
		  rs = st.executeQuery(sql);

		  while (rs.next()) {
			  ua_id = rs.getString("id");
			  aircleaner_id = rs.getInt("aircleaner_id");
			  if(ua_id.equals(user.getUser_id())==true) {
				  System.out.println("해당 아이디 정보를 찾았습니다.공기청정기 번호를 가져옵니다.");
				  System.out.println("aircleaner_id:"+aircleaner_id);
				  user.setAircleaner_id(aircleaner_id);
				  break;
			  }
			  ua_id = "empty";
			  aircleaner_id = 0;
		  }
		  
		  //user.setUser_id("empty");
		  //user.setUser_password("empty");
		  //user.setAircleaner_id(0);
        
		  rs.close();
		  st.close();
		  connection.close();
        
		  connection = DriverManager.getConnection
		  ("jdbc:mysql://localhost/final_project?useUnicode=true&characterEncoding=utf-8"
		  	+ "&&serverTimezone=UTC", "root","rusy5884!@runner");
		  
		  st = connection.createStatement();
		  sql = "select * FROM aircleaner;";
		  rs = st.executeQuery(sql);
		  
		  System.out.println(user.getAircleaner_id());
		  
		  while (rs.next()) {
			  aircleaner_id2 = rs.getInt("aircleaner_id");
			  action_motion_number = rs.getInt("action_motion_number");
			  System.out.println(aircleaner_id2+" "+ action_motion_number);
			  if(aircleaner_id2==user.getAircleaner_id()) {
				  System.out.println("해당 수동동작 관련 정보를 찾았습니다.");
				  System.out.println("aircleaner_id: "+aircleaner_id2
				  +" action_motion_number: "+action_motion_number);
				  temp_action_motion_number =action_motion_number;
				  break;
			  }
			  aircleaner_id2 = 0;
			  action_motion_number=0;
		  }
		  
		  sql = "select * FROM manual_acting_info;";
		  rs = st.executeQuery(sql);
		  
		  System.out.println(temp_action_motion_number);
		  
		  while (rs.next()) {
			  share_acting_number = rs.getInt("share_acting_number");
			  region = rs.getString("region");
			  System.out.println(share_acting_number+" "+ region);
			  if(share_acting_number==temp_action_motion_number) {
				  System.out.println("해당 지역 관련 정보를 찾았습니다.");
				  System.out.println("region: "+region);
				  user.setRegion(region);
				  return user;
			  }
			  share_acting_number = 0;
			  region="empty";
		  }
		  
		  
		  rs.close();
		  st.close();
		  connection.close();
		  
		  user.setUser_id("empty");
		  user.setUser_password("empty");
		  user.setUser_name("empty");
		  user.setPhon_number(0);
		  user.setRegion("empty");
		  user.setAircleaner_id(0);
		  
		return user;
	}
}

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class full_userInfoServlet extends HttpServlet{

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data

		//writing html in the stream  
		pw.println("<html><body>");
		pw.println("<h1>Welcome to full_userInfoServlet</h1>");
		pw.println("<ul>\n" + 
				"		<li><a href=\"/app/\">JSP</a></li>\n" + 
				"		<li><a href=\"/app/servlet-web-xml\">web.xml</a></li>\n" + 
				"		<li><a href=\"/app/servlet-addservlet\">addServlet</a></li>\n" + 
				"	</ul>");
		pw.println("</body></html>");

		pw.close();// closing the stream
	}
	
	public void doPost(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException{
		System.out.println("full_userInfoServlet");
		
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html;charset=utf-8");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data
		
		user_info user = new user_info();
		DB_Connection connection = new DB_Connection();
		
		String user_id=req.getParameter("user_id");
		
		String result ="";
		
		try {
			user = connection.Connection_Full_userInfo(user_id);
			if(user.getUser_id().equals("empty")||user.getUser_name().equals("empty")||user.getPhon_number()==0
			||user.getRegion().equals("empty")||user.getAircleaner_id()==0) {
				result= "result:NO/";
			}else {
				result="result:OK/";
			}
			
			String Phon_number_toString = Integer.toString(user.getPhon_number());
			String aircleaner_id_toString = Integer.toString(user.getAircleaner_id());
			
			result = result + user.getUser_id()+"/"+user.getUser_name()
			+"/"+Phon_number_toString+"/"+user.getRegion()+"/"+aircleaner_id_toString;
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "result:NO";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "result:NO";
		}
		
		pw.println(result);
		pw.close();
	}
	
}

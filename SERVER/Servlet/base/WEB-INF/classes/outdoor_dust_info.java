public class outdoor_dust_info {
	private String outdoor_day_time;
	private float outdoor_dust_info;
	private float outdoor_micro_dust_info;
	private String region;
	
	public String getOutdoor_day_time() {
		return outdoor_day_time;
	}
	public void setOutdoor_day_time(String outdoor_day_time) {
		this.outdoor_day_time = outdoor_day_time;
	}
	public float getOutdoor_dust_info() {
		return outdoor_dust_info;
	}
	public void setOutdoor_dust_info(float outdoor_dust_info) {
		this.outdoor_dust_info = outdoor_dust_info;
	}
	public float getOutdoor_micro_dust_info() {
		return outdoor_micro_dust_info;
	}
	public void setOutdoor_micro_dust_info(float outdoor_micro_dust_info) {
		this.outdoor_micro_dust_info = outdoor_micro_dust_info;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
	
	
}

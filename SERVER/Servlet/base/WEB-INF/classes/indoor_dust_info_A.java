public class indoor_dust_info_A {
	private String indoor_day_time;
	private float indoor_dust_info;
	private int aircleaner_number;
	
	public String getIndoor_day_time() {
		return indoor_day_time;
	}
	public void setIndoor_day_time(String indoor_day_time) {
		this.indoor_day_time = indoor_day_time;
	}
	public float getIndoor_dust_info() {
		return indoor_dust_info;
	}
	public void setIndoor_dust_info(float indoor_dust_info) {
		this.indoor_dust_info = indoor_dust_info;
	}
	public int getAircleaner_number() {
		return aircleaner_number;
	}
	public void setAircleaner_number(int aircleaner_number) {
		this.aircleaner_number = aircleaner_number;
	}
	
	
	
}

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class change_actingMotionServlet extends HttpServlet{
	public void doGet(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data

		//writing html in the stream  
		pw.println("<html><body>");
		pw.println("<h1>Welcome to change_actingMotionServlet</h1>");
		pw.println("<ul>\n" + 
				"		<li><a href=\"/app/\">JSP</a></li>\n" + 
				"		<li><a href=\"/app/servlet-web-xml\">web.xml</a></li>\n" + 
				"		<li><a href=\"/app/servlet-addservlet\">addServlet</a></li>\n" + 
				"	</ul>");
		pw.println("</body></html>");

		pw.close();// closing the stream
	}
	
	public void doPost(HttpServletRequest req,HttpServletResponse res) throws ServletException, IOException{
		System.out.println("change_actingMotionServlet");
		
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html;charset=utf-8");// setting the content type
		
		//res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data
		
		DB_Connection connection = new DB_Connection();
		
		String result = "NO";
		
		//user_id(회원 아이디), ON_OFF_checking(공기청정기 전원), manual_acting_checking(수동동작여부)
		
		String user_id=req.getParameter("user_id");
		String ON_OFF_checking = req.getParameter("ON_OFF_checking");
		//Boolean ON_OFF_checking= Boolean.valueOf(req.getParameter("ON_OFF_checking"));
		System.out.println(ON_OFF_checking);
		String manual_acting_checking=req.getParameter("manual_acting_checking");
		//Boolean manual_acting_checking =Boolean.valueOf(req.getParameter("manual_acting_checking"));
		System.out.println(manual_acting_checking);
		
		boolean ON_OFF_checking_bool = Boolean.parseBoolean(ON_OFF_checking); 
		boolean manual_acting_checking_bool = Boolean.parseBoolean(manual_acting_checking);
		try {
			result=connection.Connection_Manual_acting_info_saving(user_id, ON_OFF_checking_bool, manual_acting_checking_bool);
			pw.println("result:"+result);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			pw.println("result:"+result);
		}
		pw.close();
		
	}
}
